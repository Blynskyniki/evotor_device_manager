#!/usr/bin/env bash
!#/bin/bash

echo "Создание контейнера"
docker build -t blynskyniki/evotor_device_manager:$1 ./
echo "Тегирование контейнера "
docker tag blynskyniki/evotor_device_manager:$1  blynskyniki/evotor_device_manager:$1
echo "Заливка  контейнера"
docker push blynskyniki/evotor_device_manager:$1
echo "Готово! контейнер : blynskyniki/evotor_device_manager:$1"