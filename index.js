/* eslint-disable no-shadow */
const Server = require('./app');
const log = require('winston');
require('dotenv').config();

log.level = process.env.LOGLEVEL;


Server()
  .then(async (server) => {
    try {
      await server.start();
      log.info(`Server running at: ${server.info.uri}`);
    } catch (e) {
      throw e;
    }
  });


// Server((err, server) => {
//   if (err) {
//     throw err;
//   }
//
//   server.start((err) => {
//     if (err) {
//       throw err;
//     }
//     log.info(`Server running at: ${server.info.uri}`);
//   });
// });
