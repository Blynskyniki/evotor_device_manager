const mongo = require('./adapter');
const findOrCreate = require('mongoose-findorcreate');

const cDevicesSchema = new mongo.Schema(
  {
    uuid: {
      type: mongo.Schema.Types.ObjectId,
      ref: 'users',
    },
    pid: {
      type: String,
      required: true,
    },
    vid: {
      type: String,
      required: true,
    },
    cassa: {
      type: String,
      required: true,
    },
    port: {
      type: String,
      required: false,
    },
    store: {
      type: String,
      required: true,
    },
    duid: {
      type: mongo.Schema.Types.ObjectId,
      ref: 'device',
    },

  },

  {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updatedAt',
    },

  },
);
cDevicesSchema.plugin(findOrCreate);

module.exports = mongo.model('connectedDevices', cDevicesSchema);

