/* eslint-disable jsx-a11y/href-no-hash */
const mongo = require('./adapter');
const findOrCreate = require('mongoose-findorcreate');
const deviceSchema = new mongo.Schema(
  {
    name: {

      type: String,
      required: true,
    },
    meta: {

      vid: {
        type: String,
        //  unique : true,
        required: true,
      },
      pid: {
        type: String,
        //  unique : true,
        required: true,
      },
    },
    url: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    },

  },
);

deviceSchema.plugin(findOrCreate);
module.exports = mongo.model('device', deviceSchema);
