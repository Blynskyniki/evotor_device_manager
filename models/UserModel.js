const mongo = require('./adapter');
const findOrCreate = require('mongoose-findorcreate');


const userSchema = mongo.Schema({
  token: {
    type: String,
    required: false,
  },
  scope: {
    type: String,
  },
  evotorToken:{
    type: String,
  },
  orgid:{
    type: String,
  },
});


module.exports = mongo.model('users', userSchema);

