const { defineSupportCode } = require('cucumber');


let srv;
let token;
let StatusCode;
let userName;
defineSupportCode(({ When }) => {
  // Synchronous

  When('Я запускаю сервер', (cb) => {
    require('../../app')((err, start) => {
      if (err) {
        cb(err);
      }

      srv = start;
      cb();
    });
  });

  When('Я очищаю переменные', (cb) => {
    const srv = null;
    const token = null;
    const StatusCode = null;
    const userName = null;
    cb();
  });


  When('Я ожидаю код {string}', (code, cb) => {
    if (code == StatusCode) {
      cb();
    } else {
      cb(new Error(`Неверный статус код (${StatusCode}), ожидался ${code}`));
    }
  });


  When('Я регистрирую пользователя с ролью {string}', (role, cb) => {
    userName = `user${Math.random() * (10 - 1)}${1}`;
    console.log(userName);
    const post = {
      name: userName,
      role,
    };
    const options = {
      method: 'POST',
      url: '/create/user',
      payload: post,
    };


    srv.inject(options, (res) => {
      if (res) {
        console.log(res.result);
        StatusCode = res.statusCode;
        token = res.result.credentials.token;
        console.log(`TOKEN = ${token}`);
        cb();
      } else {
        cb(new Error('Нет ответа от сервера'));
      }
    });
  });

  When('Я повторно региструю пользователя с ролью {string}', (role, cb) => {
    console.log(userName);
    const post = {
      name: userName,
      role,
    };
    const options = {
      method: 'POST',
      url: '/create/user',
      payload: post,
    };


    srv.inject(options, (res) => {
      if (res) {
        console.log(res.result);
        StatusCode = res.statusCode;
        token = res.result.credentials.token;
        console.log(`TOKEN = ${token}`);
        cb();
      } else {
        cb(new Error('Нет ответа от сервера'));
      }
    });
  });


  When('Я Ищу пользователя в базе данных', (cb) => {
    const options = {
      method: 'get',
      url: `/find/user/${userName}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    srv.inject(options, (res) => {
      if (res) {
        console.log(res.result);

        StatusCode = res.statusCode;
        console.log(StatusCode);
        cb();
      } else {
        cb(new Error('Нет ответа от сервера'));
      }
    });
  });

  When('Я удаляю пользователя', (cb) => {
    const options = {
      method: 'delete',
      url: `/delete/user/${userName}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    srv.inject(options, (res) => {
      if (res) {
        console.log(res.result);

        StatusCode = res.statusCode;
        console.log(StatusCode);
        cb();
      } else {
        cb(new Error('Нет ответа от сервера'));
      }
    });
  });


  When('Я добавляю в справочник устройство с pid {string},vid {string},name {string}', (pid, vid, name, cb) => {
    const post = {
      name,
      vid,
      pid,
      url: 'http://driver.fake.url.com',
    };
    const options = {
      method: 'post',
      url: '/create/device/',
      payload: post,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    srv.inject(options, (res) => {
      if (res) {
        console.log(res.result);

        StatusCode = res.statusCode;
        console.log(StatusCode);
        cb();
      } else {
        cb(new Error('Нет ответа от сервера'));
      }
    });
  });


  When('Я Ищу устройство в справочнике с pid {string},vid {string}', (pid, vid, cb) => {
    const options = {
      method: 'get',
      url: `/find/device/${vid}/${pid}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    srv.inject(options, (res) => {
      if (res) {
        console.log(res.result);

        StatusCode = res.statusCode;
        console.log(`Status code = ${StatusCode}`);
        cb();
      } else {
        cb(new Error('Нет ответа от сервера'));
      }
    });
  });


  When('Я Удаляю устройство из справочника с pid {string},vid {string}', (pid, vid, cb) => {
    const options = {
      method: 'delete',
      url: `/delete/device?vid=${vid}&pid=${pid}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    srv.inject(options, (res) => {
      if (res) {
        console.log(res.result);

        StatusCode = res.statusCode;
        console.log(StatusCode);
        cb();
      } else {
        cb(new Error('Нет ответа от сервера'));
      }
    });
  });

  When('Эвотор сообщает о подключениии устройства с pid {string},vid {string}, номер кассы {string}', (xpid, xvid, cid, cb) => {
    const options = {
      method: 'POST',
      url: '/api/v1/data/orig',
      headers: {
         Authorization: `Bearer ${token}`,
        'x-evotor-device-uuid': cid,
        'x-evotor-store-uuid': '777',
      },
      payload: {
        pid: xpid,
        vid: xvid,
        port: '001',
      },
    };
    srv.inject(options, (res) => {
      if (res) {
        console.log(res.result);

        StatusCode = res.statusCode;
        console.log(StatusCode);
        cb();
      } else {
        cb(new Error('Нет ответа от сервера'));
      }
    });
  });

  When('Я удаляю из лога устройство  подключенное к эвотору с pid {string},vid {string}, номер кассы {string}', (pid, vid, cid, cb) => {
    const options = {
      method: 'delete',
      url: `/delete/connect/device/${vid}/${pid}/${cid}`,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    srv.inject(options, (res) => {
      if (res) {
        console.log(res.result);

        StatusCode = res.statusCode;
        console.log(StatusCode);
        cb();
      } else {
        cb(new Error('Нет ответа от сервера'));
      }
    });
  });

  When('Я Ищу подключенные устройства для данного пользователя', (cb) => {
    const options = {
      method: 'get',
      url: '/find/connected/device',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    };
    srv.inject(options, (res) => {
      if (res) {
        console.log(res.result);

        StatusCode = res.statusCode;
        console.log(StatusCode);
        cb();
      } else {
        cb(new Error('Нет ответа от сервера'));
      }
    });
  });
});
