/* eslint-disable no-unused-expressions,no-underscore-dangle,no-shadow */
const log = require('winston');
const dModel = require('../models/DeviceModel');
const connectModel = require('../models/CDevicesModel');
const req = require('request-promise');

log.level = process.env.LOGLEVEL;


const device = {};

// --------------------Создание устройства
device.create = async (request, reply) => {
  log.debug('Routing.', 'Create device', 'Start');
  try {
    const exist = await dModel.findOne({
      meta: {
        vid: request.payload.vid,
        pid: request.payload.pid,
      },
    });
    if (!exist) {
      const created = await dModel.findOrCreate({
        name: request.payload.name,
        meta: {
          vid: request.payload.vid,
          pid: request.payload.pid,
        },
        url: request.payload.url,
      });

      log.debug('New device :', JSON.stringify(created, '', ' '));
      reply(created).code(200);
      return;
    }
    log.debug('Exist  device :', JSON.stringify(exist, '', ' '));
    reply({
      message: 'Already exists',
      obj: exist,
    }).code(409);
    return;
  } catch (err) {
    log.error('Error.', err);
    reply({ message: `Error. ${err}` }).code(503);
  }
};
// --------------------Изменение устройства
device.update = async (request, reply) => {
  log.debug('Routing.', 'Update device', 'Start');
  try {
    const conditions = {
      meta: {
        vid: request.params.findVid,
        pid: request.params.findPid,
      },
    };
    const newDeviceMeta = {
      vid: request.payload.vid,
      pid: request.payload.pid,
    };
    request.payload.meta = newDeviceMeta;
    const updated = await dModel.update(conditions, request.payload, { multi: false });
    if (updated) {
      log.debug('Device updated :', JSON.stringify(updated, '', ' '));
      reply(updated).code(200);
    } else {
      log.error('Device not found');
      reply({ message: 'Device not found' }).code(404);
    }
  } catch (err) {
    log.error('Error.', err);
    reply({ message: `Error. ${err}` }).code(503);
  }
};
// --------------------Удаление устройства
device.deleteByPidVid = async (request, reply) => {
  log.debug('Routing.', 'Delete', 'Start');
  try {
    const deleted = await dModel.findOneAndRemove({
      meta: {
        vid: request.query.vid,
        pid: request.query.pid,
      },
    });
    if (deleted) {
      log.debug('Device deleted :', JSON.stringify(deleted, '', ' '));
      reply(deleted).code(200);
    } else {
      log.debug('Device not found');
      reply({ message: 'Device not found' }).code(404);
    }
  } catch (err) {
    log.error('Error.', err);
    reply({ message: `Error. ${err}` }).code(503);
  }
};

// --------------------Поиск устройства
device.findByPidVid = async (request, reply) => {
  log.debug('Routing.', 'Find', 'Start');
  try {
    const record = await dModel.findOne({
      meta: {
        vid: request.params.vid,
        pid: request.params.pid,
      },
    });
    if (record) {
      log.debug('Find device :', record.name);
      reply(record).code(200);
    } else {
      log.debug('Device not found');
      reply({ message: 'Device not found' }).code(404);
    }
  } catch (err) {
    log.error('Error.', err);
    reply({ message: `Error. ${err}` }).code(503);
  }
};
// --------------------Поиск всех устройств
device.findAll = async (request, reply) => {
  log.debug('Routing.', 'Find All', 'Start');
  try {
    const records = await dModel.find();
    if (records) {
      log.debug('Find devices count  :', records.length);
      reply(records).code(200);
    } else {
      log.debug('Devices not found');
      reply({}).code(200);
    }
  } catch (err) {
    log.error('Error.', err);
    reply({ message: `Error. ${err}` }).code(503);
  }
};
// --------------------Выборка устройств по пользователю устройства
device.selectConncectDevices = async (request, reply) => {
  log.debug('select Conncect Devices', 'Start');
  const optionsStore = {
    uri: 'https://api.evotor.ru/api/v1/inventories/stores/search',
    headers: {
      'User-Agent': 'request',
      'X-Authorization': request.auth.credentials.user.evotorToken
    },
    json: true, // Automatically parses the JSON string in the response
  };
const optionsCass = {
    uri: 'https://api.evotor.ru/api/v1/inventories/devices/search',
    headers: {
      'User-Agent': 'request',
      'X-Authorization': request.auth.credentials.user.evotorToken
    },
    json: true, // Automatically parses the JSON string in the response
  };

  try {
    const stores = await req(optionsStore);
    console.log(stores);
    const cass = await req(optionsCass);
    console.log(cass);
    const usbDevices = await connectModel.find({
      uuid: request.auth.credentials.user._id,
    }).populate('uuid').exec();
    if (!usbDevices) {
      log.error('Error.', 'No connected devices');
      reply({}).code(200);
    }

    for (const usbDevice in usbDevices) {
      const tmp = await dModel.findOne({
        'meta.pid': usbDevices[usbDevice].pid,
        'meta.vid': usbDevices[usbDevice].vid,
      });

      for (const store in stores) {
        if (stores[store].uuid === usbDevices[usbDevice].store) {
          usbDevices[usbDevice].set('store', stores[store].name);
        }
      }
      for (const cassa in cass) {
        if (cass[cassa].uuid === usbDevices[usbDevice].cassa) {
          usbDevices[usbDevice].set('cassa', cass[cassa].name);
        }
      }

      usbDevices[usbDevice].set('duid', tmp);
    }

    log.debug(JSON.stringify(usbDevices, '', ' '));
    reply(usbDevices);
  } catch (err) {
    log.error('Error.', err);
    reply({ message: `Error. ${err}` }).code(503);
  }
};
// --------------------Удаление подключенных устройств
device.deleteConncectDevice = async (request, reply) => {
  log.debug('Delete connect device', 'Start');
  try {
    const deleted = await connectModel.findOneAndRemove({
      cassa: request.params.cid,
      uuid: request.auth.credentials.user,
      duid: device,

    });
    if (deleted) {
      log.debug('Device deleted :', JSON.stringify(deleted, '', ' '));
      reply(deleted).code(201);
    } else {
      log.debug('Device not found');
      reply({ message: 'Device not found' }).code(404);
    }
  } catch (err) {
    log.error('Error.', err);
    reply({ message: `Error. ${err}` }).code(503);
  }
};
// --------------------Сохранение подключенного  устройства
device.saveConnectDeviceTest = (request, reply) => {
  log.debug('Routing.', 'Save connected devices', 'Start');
  log.debug('Headers', JSON.stringify(request.headers, '', ' '));
  log.debug('PAyload', JSON.stringify(request.payload, '', ' '));
  log.debug('Auth', JSON.stringify(request.auth, '', ' '));
  reply('ok');
};

device.saveConnectDevice = async (request, reply) => {
  log.debug('Routing.', 'Save connected devices', 'Start');
  log.debug('Headers', JSON.stringify(request.headers, '', ' '));
  log.debug('PAyload', JSON.stringify(request.payload, '', ' '));
  log.debug('Auth', JSON.stringify(request.auth, '', ' '));
  try {
    const find = await connectModel.findOne({
      uuid: request.auth.credentials.user,
      pid: request.payload.pid,
      vid: request.payload.vid,
    });
    if (!find) {
      const usbdevice = await connectModel.create({
        uuid: request.auth.credentials.user,
        pid: request.payload.pid,
        vid: request.payload.vid,
        cassa: request.headers['x-evotor-device-uuid'],
        store: request.headers['x-evotor-store-uuid'],
      });
      log.debug('New connect device :', JSON.stringify(usbdevice, '', ' '));
      reply(usbdevice).code(200);
      return;
    }
    log.debug('Device already exist :', JSON.stringify(find, '', ' '));
    reply(find).code(200);
  } catch (err) {
    log.error('Error.', err);
    reply({ message: `Error. ${err}` }).code(503);
  }
};
module.exports = device;
