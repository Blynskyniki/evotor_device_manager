/* eslint-disable no-multiple-empty-lines */
const log = require('winston');
const uModel = require('../models/UserModel');
require('dotenv').config();

log.level = process.env.LOGLEVEL;
const user = {};

// --------------------Создание устройства
user.create = async (request, reply) => {
  log.debug('Routing.', 'Create user ', 'Start');
  log.debug('Credentials.',JSON.stringify(request.payload));
  try {
    const ExistUser = await user.isNotExistUser(request.payload.token);

    if (!ExistUser) {
      const role = request.payload.role ? request.payload.role : 'user';
      request.payload.scope = role;

      const newUser = await uModel.create(request.payload);
      log.debug('New user :', newUser.token);
      reply(newUser).code(201);
      return;
    }
    reply(ExistUser).code(200);
    return;
  } catch (err) {
    log.error('Error.', err);
    reply({ message: `Error. ${err}` }).code(503);
  }
};


// --------------------Костыль админской авторизации
user.auth = async (request, reply) => {
  if (request.payload.name === process.env.ADMINNAME) {
    if (request.payload.password === process.env.ADMINPASSWORD) {
      request.payload.role = 'admin';
      try {
        const ExistUser = await user.isNotExistUser(process.env.ADMINTOKEN);

        if (!ExistUser) {
          const role = request.payload.role ? request.payload.role : 'user';
          const newUser = await uModel.create({
            name: request.payload.name,
            token: process.env.ADMINTOKEN,
            scope: role,
          });
          log.debug('New user :', newUser.token);
          reply(newUser).code(201);
          return;
        }
        reply(ExistUser).code(200);
      } catch (err) {
        log.error('Error.', err);
        reply({ message: `Error. ${err}` }).code(503);
      }
    } else {
      reply({
        message: 'Неверный логин или пароль',
      }).code(403);
    }
  } else {
    reply({
      message: 'Неверный логин или пароль',
    }).code(403);
  }
};



user.delete = async (request, reply) => {
  log.debug('Routing.', 'Delete user', 'Start');
  try {
    const delleteUser = await uModel.remove(request.payload);
    reply(delleteUser).code(200);
  } catch (err) {
    log.error('Error.', err);
    reply({ message: `Error. ${err}` }).code(503);
  }
};

user.isNotExistUser = async (n) => {
  log.debug('Is exist User.', 'Start');
  try {
    const findUser = await uModel.findOne({
      token: n,
    });

    if (findUser) {
      log.debug('Is exist User ', ' Find user :', JSON.stringify(findUser, '', ' '));
      return findUser;
    }

    return undefined;
  } catch (err) {
    log.error('Error.', err);
    return err;
  }
};

user.validate = async (token, cb) => {
  log.debug('Routing.', 'validate', 'Start');
  log.debug('Credentials',JSON.stringify(token,'',' ',))

  try {
    const user = await uModel.findOne({ token:token });
    if (user) {
      log.debug('Find user :', JSON.stringify(user,'', ''));

      if (user.token === token) {
        log.debug('Validate user :', 'TRUE');


        cb(null, true, {
          user,
          token,
          scope: user.scope,
        });
        return;
      }
      log.debug('Validate user :',  'FALSE', JSON.stringify({t:token}));
      cb(null, false);
      return;
    }
    log.debug('Validate user :',  'FALSE',JSON.stringify({t:token}));
    cb(null, false);
    return;
  } catch (err) {
    log.error('Error.', err);
    cb(err, false);
  }

};

module.exports = user;
