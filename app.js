/* eslint-disable global-require */
const Hapi = require('hapi');
const log = require('winston');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('./package');
const Auth = require('./plugins/auth');
require('dotenv').config();


// log.level = process.env.LOGLEVEL;


module.exports = async () => {
  const server = new Hapi.Server();
  server.connection({
    port: process.env.PORT,
    routes: { cors: true },
  });


  await server.register([
    Auth,
    Inert,
    Vision,
    {
      register: HapiSwagger,
      options: {
        info: {
          title: `${Pack.name} API Documentation`,
          version: Pack.version,
        },
        securityDefinitions: {
          Bearer: {
            type: 'apiKey',
            name: 'Authorization',
            in: 'header',
          },
        },
        security: [{ Bearer: [] }],
      },
    },

  ]);

  server.route(require('./routes/router'));

  return server;
};

