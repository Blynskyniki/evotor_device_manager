/* eslint-disable import/no-extraneous-dependencies */

const device = require('./../controllers/cDevice');
const user = require('./../controllers/cUser');
const Joi = require('joi');

const responseCodes = {
  'hapi-swagger': {
    responses: {
      200: { description: 'Выполнено' },
      201: { description: 'Выполнено' },
      404: { description: 'Не найдено' },
      503: { description: 'Что-то сломалось :-(' },
      401: { description: 'Не авторизован' },
      400: { description: 'Неправильный токен' },
      409: { description: 'Конфликт данных' },
    },
  },
};
const routes = [

  // -------------- Создание устройства
  {
    method: 'POST',
    path: '/create/device',
    handler: device.create,
    config: {
      description: 'Добавление драйвера в справочник',
      auth: {
        strategy: 'bearer',
        scope: ['admin'],
      },
      tags: ['api'],
      plugins: responseCodes,
      validate: {
        payload: {
          vid: Joi.string().min(1).required(),
          name: Joi.string().min(4).required(),
          pid: Joi.string().min(1).max(10).required(),
          url: Joi.string().uri({ scheme: ['http', 'https'] }).required(),
        },
      },
    },
  },
  // -------------- Изменения устройства
  {
    method: 'PUT',
    path: '/update/{findVid}/{findPid}/device',
    handler: device.update,
    config: {
      description: 'Обновление данных о драйвере в справочнике',
      auth: {
        strategy: 'bearer',
        scope: ['admin'],
      },
      tags: ['api'],
      plugins: responseCodes,
      validate: {
        params: {
          findVid: Joi.string().min(1).required(),
          findPid: Joi.string().min(1).required(),
        },
        payload: {
          vid: Joi.string().min(1).required(),
          name: Joi.string().min(4).required(),
          pid: Joi.string().min(1).max(10).required(),
          url: Joi.string().uri({ scheme: ['http', 'https'] }).required(),
        },
      },
    },
  },
  // -------------- Удаление  устройства
  {
    method: 'delete',
    path: '/delete/device',
    handler: device.deleteByPidVid,
    config: {
      description: 'Удаление драйвера из справочника',
      auth: {
        strategy: 'bearer',
        scope: ['admin'],
      },
      tags: ['api'],
      plugins: responseCodes,
      validate: {
        query: {
          vid: Joi.string().min(1).required(),
          pid: Joi.string().min(1).required(),
        },
      },
    },
  },
  // -------------- Поиск устройства
  {
    method: 'get',
    path: '/find/device/{vid}/{pid}',
    handler: device.findByPidVid,
    config: {
      description: 'Поиск драйвера по PID и VID',

      auth: {
        strategy: 'bearer',
        scope: ['user', 'admin'],
      },
      tags: ['api', 'search'],
      plugins: responseCodes,
      validate: {
        params: {
          vid: Joi.string().min(1).required(),
          pid: Joi.string().min(1).required(),
        },
      },
    },
  },
  // -------------- Поиск всех  устройств
  {
    method: 'get',
    path: '/find/device',
    handler: device.findAll,
    config: {
      description: 'Показать все драйвера',
      plugins: responseCodes,
      auth: {
        strategy: 'bearer',
        scope: ['user', 'admin'],
      },
      tags: ['api', 'search'],
    },
  },
  // -------------- Удаление пользователя
  {
    method: 'POST',
    path: '/delete/user',
    handler: user.delete,
    config: {
      description: 'Удаление пользователя',
      tags: ['api'],
      plugins: responseCodes,
      validate: {
        payload: {
          evotorToken: Joi.string(),
          token: Joi.string().required(),
          orgId: Joi.string(),
        },
      },
    },
  },
  // -------------- Создание пользователя
  {
    method: 'POST',
    path: '/create/user',
    handler: user.create,
    config: {
      tags: ['api'],
      description: 'Создание пользователя',
      plugins: responseCodes,
      validate: {
        payload: {
          evotorToken: Joi.string().example('534785689568568568'),
          role: Joi.string().example('user'),
          token: Joi.string().required().example('123321123321'),
          orgId: Joi.string().example('12345'),
        },
      },
    },
  },

  // -------------- Фейковое подключение устройства
  {
    method: 'POST',
    path: '/api/v1/data/1',
    handler: device.saveConnectDevice,
    config: {
      //tags: ['api'],
    },
  },
  // -------------- Авторизация админа
  {
    method: 'POST',
    path: '/auth',
    handler: user.auth,
    config: {
      tags: ['api'],
      description: 'Авторизация Админа, Логин и пароль в переменных окружения',
      plugins: responseCodes,
      validate: {
        payload: {
          name: Joi.string().min(1).required(),
          password: Joi.string().min(1).required(),

        },
      },
    },
  },
  // -------------- Сохранение данных
  {
    method: 'POST',
    path: '/api/v1/data',
    handler: device.saveConnectDevice,
    config: {
      tags: ['api'],
      description: 'Запрос от Эвотора к серверу, сообщающий подключенные устройства',
      plugins: responseCodes,
      auth: {
        strategy: 'bearer',
        scope: ['user', 'admin'],
      },
      validate: {
        headers: Joi.object({
          'x-evotor-device-uuid': Joi.string().required(),
          'x-evotor-store-uuid': Joi.string().required(),
        }).options({
          allowUnknown: true,
        }),
        payload: Joi.object({
          vid: Joi.string().min(1).required(),
          pid: Joi.string().min(1).required(),
          port: Joi.string().min(1),
        }).example({
          vid: '2352352135',
          pid: '123321',
          port: '005',
        }),

      },
    },
  },
  // -------------- Вывод подключенных устройств
  {
    method: 'get',
    path: '/find/connected/device',
    handler: device.selectConncectDevices,
    config: {
      description: 'Показать подключенные устройства',
      tags: ['api'],
      plugins: responseCodes,
      auth: {
        strategy: 'bearer',
        scope: ['user', 'admin'],
      },

    },
  },
  // -------------- Удаление подключенных устройств
  {
    method: 'delete',
    path: '/delete/connect/device/{vid}/{pid}/{cid}',
    handler: device.deleteConncectDevice,
    config: {
      description: 'Запрос от Эвотора к серверу, сообщающий отключенные устройства',
      tags: ['api'],
      plugins: responseCodes,
      validate: {
        params: {
          cid: Joi.string().min(1).max(20).required(),
          vid: Joi.string().min(1).required(),
          pid: Joi.string().min(1).required(),
        },
      },
      auth: {
        strategy: 'bearer',
        scope: ['admin'],
      },

    },
  },
];


module.exports = routes;
