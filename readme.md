----------------------
Менеджер драйверов
----------------------

//-------------- Создание устройства

      method: 'POST',
      path: '/create/device/',

//-------------- Удаление  устройства

      method: 'delete',
      path: '/delete/device/{vid}/{pid}',

//-------------- Поиск устройства

      method: 'get',
      path: '/find/device/{vid}/{pid}',

//-------------- Создание пользователя

      method: 'POST',
      path: '/create/user',

//-------------- Информация о пользователе

      method: 'get',
      path: '/find/user/{name}',

//-------------- Удаление пользователя

      method: 'delete',
      path: '/delete/user/{name}',

