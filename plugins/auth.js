/* eslint-disable global-require */


exports.register = (plugin, options, next) => {
  plugin.register({ register: require('hapi-auth-bearer-token') }, (err) => {
    if (err) throw err;
    plugin.auth.strategy('bearer', 'bearer-access-token', {
      allowQueryToken: true, // optional, false by default
      allowMultipleHeaders: true, // optional, false by default
      accessTokenName: 'access_token', // optional, 'access_token' by default
      validateFunc: require('../controllers/cUser').validate,
    });

    next();
  });
};

exports.register.attributes = {
  pkg: {
    name: 'authentication',
    description: 'This is the authentication provider',
    main: 'auth.js',
    author: 'John Brett',
    license: 'MIT',
  },
};

